package ro.robert.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ro.robert.entities.Note;
import ro.robert.entities.dao.NoteRepository;


@Component
public class DataLoader implements ApplicationRunner {

    private NoteRepository noteRepository;

    @Autowired
    public DataLoader(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public void run(ApplicationArguments args) {
        noteRepository.save(new Note(Long.valueOf(1), "Lorem Ipsum", "Lorem Ipsum este pur şi simplu o machetă pentru text a industriei tipografice. Lorem Ipsum a fost macheta standard a industriei încă din secolul al XVI-lea, când un tipograf anonim a luat o planşetă de litere şi le-a amestecat pentru a crea o "));
        noteRepository.save(new Note(Long.valueOf(2), "De ce il folosinm?", "E un fapt bine stabilit că cititorul va fi sustras de conţinutul citibil al unei pagini atunci când se uită la aşezarea în pagină. Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin no"));
        noteRepository.save(new Note(Long.valueOf(3), "De unde provine", "În ciuda opiniei publice, Lorem Ipsum nu e un simplu text fără sens. El îşi are rădăcinile într-o bucată a literaturii clasice latine din anul 45 î.e.n., făcând-o să aibă mai bine de 2000 ani. Profesorul universitar de latină de la c"));
    }
}
