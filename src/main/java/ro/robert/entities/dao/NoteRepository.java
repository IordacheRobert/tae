package ro.robert.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.robert.entities.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {
}
