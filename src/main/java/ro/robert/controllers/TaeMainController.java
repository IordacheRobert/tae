package ro.robert.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ro.robert.entities.Note;
import ro.robert.entities.dao.NoteRepository;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;


@Controller
@RequestMapping()
public class TaeMainController {

    private NoteRepository noteRepository;

    @Autowired
    public TaeMainController(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @GetMapping()
    public String getInside(Model model){
        model.addAttribute("note", new Note());
        return "mainPage";
    }

    @GetMapping("/notes")
    public ResponseEntity<List<Note>> getAllNotes(){
        return new ResponseEntity<List<Note>>(noteRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/notes/{id}")
    public String getNote(@PathVariable("id") Long id, Model model){
        model.addAttribute("note", noteRepository.findOne(id));
        return "edit";
    }

    @PostMapping("/notes")
    @Transactional
    public String addNote(@ModelAttribute Note note) {
        noteRepository.save(note);
        return "redirect:/";
    }

    @DeleteMapping("/notes/{id}")
    @Transactional
    public String deleteNote(@PathVariable("id") Long id) {
        noteRepository.delete(id);
        return "redirect:/";
    }


    @PostMapping("/notes/{id}/save")
    @Transactional
    public String saveNote(@PathVariable("id") Long id, @ModelAttribute Note note, HttpSession session) {
        Note persistedNote = noteRepository.findOne(id);
        persistedNote.setTitle(note.getTitle());
        persistedNote.setContent(note.getContent());
        return "redirect:/";
    }

}
