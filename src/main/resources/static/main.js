$(document).ready(function () {

    loadInfo();

});

function loadInfo(){
    $.ajax({
        type: "GET",
        url: "/notes",
        contentType: "application/json",
        dataType: 'json',
        success: function (data) {

            console.log(data);
                data.forEach(function(note){
                    console.log(note);
                    $('#noteList').append("<a href='/notes/"+ note.id+"' class='list-group-item'>" +
                        "<h4 class='list-group-item-heading'>"+note.title+"</h4>" +
                            // "<button type='button' class='btn btn-default' onclick='deleteNote("+note.id+")'>Delete</button>" +
                        "<p class='list-group-item-text'>"+note.content+"</p>" +
                        "<form action='notes/"+ note.id +"' method='post'>" +
                        "<input type='hidden' name='_method' value='delete'>" +
                        "<input type='submit' value='Remove' />" +
                        "</form>" +
                        "</a>")
                });

        },
        error: function (e) {
            console.log(data);
        }
    });
}

function deleteNote(id){
    $.ajax({
        type: "DELETE",
        url: "/notes/"+id,
        success: function (data) {
            console.log(data);
        },
        error: function (e) {
            console.log(e);
        }
    });
}